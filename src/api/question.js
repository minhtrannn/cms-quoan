import request from '@/utils/request';
export function getQuestionByAnswer(answerId) {
  return request({
    url: '/qa/get-question-id',
    method: 'post',
    data: {
      answer_id: answerId
    }
  });
}

export function getQuestionByUser(params) {
  return request({
    url: '/qa/question-answer-by-user',
    method: 'get',
    params: params
  });
}

export function getQuestionGuruByUser(params) {
  return request({
    url: '/guru/cms/question-by-user',
    method: 'get',
    params: params
  });
}