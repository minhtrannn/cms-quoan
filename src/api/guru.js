import request from '@/utils/request';
export function questionStatistic(query={},token="") {
  let url = '/guru/cms/question-statistic';
  if (!token){
    return request({
      url: url,
      method: 'get',
      params: query
    });
  } else {
    return request({
      url: url,
      method: 'get',
      params: query,
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }
}
export function guruQuestionStatistic(query={},token="") {
  let url = '/guru/cms/guru-question-statistic';
  if (!token){
    return request({
      url: url,
      method: 'get',
      params: query
    });
  } else {
    return request({
      url: url,
      method: 'get',
      params: query,
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }
}
export function guruStatistic(token="") {
  if (!token){
    return request({
      url: '/profile/guruLst',
      method: 'get'
    });
  } else {
    return request({
      url: '/profile/guruLst',
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }
}
export function resetGuruQuestion(data) {
  return request({
    url: '/guru/cms/reset-guru-question',
    method: 'post',
    data: data
  });
}
