import request from '@/utils/request';

export function login(data) {
  return request({
    url: 'login',
    method: 'post',
    data: data,
  });
}

export function getInfo(token="") {
  if (!token){
    return request({
      url: 'user',
      method: 'get'
    });
  } else {
    return request({
      url: 'user',
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post',
  });
}
