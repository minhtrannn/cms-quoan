import request from '@/utils/request';

export function userHourlyActivity(date) {
  return request({
    url: '/report/user/hourlyActivity',
    method: 'get',
    params: {
      date: date
    },
  });
}

export function userDailyActivity(params) {
  return request({
    url: '/report/user/dailyActivity',
    method: 'get',
    params: params,
  });
}

export function userRegister(params) {
  return request({
    url: '/report/user/register',
    method: 'get',
    params: params,
  });
}