import request from '@/utils/request';

export function saveData(servicePath, methodType, data) {
  return request({
    url: servicePath,
    method: methodType,
    data: data
  });
}
