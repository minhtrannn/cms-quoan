/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const userRoutes = {
  path: '/user',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Quản lý người dùng',
    icon: 'peoples',
    roles: 2
  },
  children: [
    {
      path: 'list',
      name: 'UserListPage',
      component: () => import('@/views/user/index'),
      meta: { title: 'Danh sách người dùng', noCache: true },
    },
    {
      path: 'detail/:id',
      component: () => import('@/views/user/detail'),
      name: 'UserDetailPage',
      meta: { title: 'Chi tiết người dùng' },
      hidden: true,
    },
  ],
};

export default userRoutes;
