import Layout from '@/layout';

const facultyRoutes = {
  path: '/faculty',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Quản lý khoa',
    icon: 'list',
    roles: 2
  },
  children: [
    {
      path: 'list',
      name: 'FacultyList',
      component: () => import('@/views/faculty/index'),
      meta: { title: 'Danh sách khoa', noCache: true },
    },
  ],
};

export default facultyRoutes;