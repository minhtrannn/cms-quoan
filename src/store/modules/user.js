import { login, getInfo, getRoleUser } from '@/api/auth';
import { isLogged, setLogged, removeToken } from '@/utils/auth';
import router, { resetRouter } from '@/router';

const state = {
  id: null,
  user: null,
  token: isLogged(),
  name: '',
  avatar: '',
  roles: [],
};

const mutations = {
  SET_ID: (state, id) => {
    state.id = id;
  },
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
  },
  SET_AUTHENTICATED: (state, authenticated) => {
    state.authenticated = authenticated;
  },
};

const actions = {
  // user login
  login({ commit }, params) {
    return new Promise((resolve, reject) => {
    login(params)
        .then(response => {
            const { data } = response;
            commit('SET_TOKEN', data);
            setLogged(data);
            resolve();
        })
        .catch(error => {
            reject(error);
        });
    })
},


  getInfo({ commit }) {
    return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
            const { data } = response;
            if (!data) {
                reject('Verification failed, please Login again.');
            }

            const { id, name, roles, avatar } = data;
            // commit('SET_ID', id);
            // commit('SET_NAME', name);
            // commit('SET_ROLES', roles);
            // commit('SET_AUTHENTICATED', true);
            // commit('SET_AVATAR', avatar);
            // resolve(data);
            if(roles == 2)
            {
              commit('SET_ID', id);
              commit('SET_NAME', name);
              commit('SET_ROLES', roles);
              commit('SET_AUTHENTICATED', true);
              commit('SET_AVATAR', avatar);
              resolve(data);
            }
            else 
            {
              reject("Bạn không được cấp phép vào trang này");
            }
        })
        .catch(error => {
            reject(error);
        });
    });
},

  // user logout
  logout({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      resetRouter();
      resolve();
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      resolve();
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
