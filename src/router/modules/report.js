/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const reportRoutes = {
  path: '/report',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Quản lý vi phạm',
    icon: 'bug',
    roles: 2
  },
  children: [
    {
      path: 'list',
      name: 'ReportListPage',
      component: () => import('@/views/report/index'),
      meta: { title: 'Danh sách vi phạm', noCache: true },
    },
  ],
};

export default reportRoutes;
