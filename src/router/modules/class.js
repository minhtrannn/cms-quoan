import Layout from '@/layout';

const classRoutes = {
  path: '/class',
  component: Layout,
  redirect: 'noredirect',
  hidden: false,
  meta: {
    title: 'Quản lý môn học',
    icon: 'list',
    roles: 2
  },
  children: [
    {
      path: 'list',
      name: 'ClassList',
      component: () => import('@/views/class/index'),
      meta: { title: 'Danh sách môn học', noCache: true },
    },
  ],
};

export default classRoutes;