import request from '@/utils/request';

export function getScore(userId) {
  return request({
    url: '/score-management/score-info',
    method: 'get',
    params: {
      userId: userId,
      pointTypeCode: 'DEFAULT'
    }
  });
}

export function getQaDetail(userId) {
  return request({
    url: '/qa/statistic-by-user',
    method: 'get',
    params: {
      user_id: userId
    }
  });
}